CC = `curl-config --cc`
CURLLIBS = `curl-config --libs`

tft: tft.o
	$(CC) -g -o $@ $< $(CURLLIBS) -ljson-c -lncursesw

tft.o: tft.c
	$(CC) -g -c $<

clean:
	rm -f tft.o tft data/games.json
