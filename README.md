# Twitch from Terminal

A twitch terminal browser written in C with ncurses. Browse the world's most premiere game streaming service all from the comfort of your terminal.

## Installation
Get dependencies:
```bash
pacman -S vlc streamlink ncurses curl lib32-libcurl-gnutls lib32-json-c 
```
## License
[GNU GPLv3](https://gitlab.com/watermelonvotary/twitch-from-terminal/-/blob/master/LICENSE)