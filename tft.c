#include "tft.h"
#include <curl/curl.h>
#include <json-c/json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>


/* size_t writeFunction(void* ptr, size_t size, size_t nmemb, char*[] data) { */
/*   data->append((char*)ptr, size * nmemb); */
/*   return size * nmemb; */
/* } */

int queryTwitch();
void topGameParser(struct gameData gameArray[gameLimit]);
void streamParser(struct streamData stream);
void cursesInit(struct gameData gameArray[gameLimit]);
void printGames(WINDOW *gameList, struct gameData gameArray[gameLimit]);
void printHelp(WINDOW *helpMenu, int wyMax, int wxMax);
int streamlinkConnect(struct streamData stream);


int queryTwitch() {
  curl_global_init(CURL_GLOBAL_DEFAULT);
  CURL *curl;
  CURLcode res;
  FILE *jf;
  int result;
  curl = curl_easy_init();

  jf = fopen("data/games.json", "wb");

  if(curl) {
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, acceptHeader);
    headers = curl_slist_append(headers, clientIDHeader);
    
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, topGamesURL);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, jf);
    res = curl_easy_perform(curl);
    fclose(jf);
    curl_slist_free_all(headers);
    if(res = CURLE_OK) {
    }
    else {
      //fprintf(stderr, "curl_easy_perform() returned %s\n",curl_easy_strerror(res));      
    }
  }
  curl_easy_cleanup(curl);
  curl_global_cleanup();
  return 0;
}

void topGameParser(struct gameData gameArray[gameLimit]) {
  int i;
  struct json_object *gameObj, *nameObj, *viewersObj, *channelObj, *IDObj;
  struct json_object *twitchObj, *topArray, *topArrayObj;
  
  twitchObj = json_object_from_file("data/games.json");
  topArray = json_object_object_get(twitchObj, "top");

  for(i = 0; i < gameLimit; i++) {
    topArrayObj = json_object_array_get_idx(topArray, i);
    gameObj     = json_object_object_get(topArrayObj, "game");
    nameObj     = json_object_object_get(gameObj, "localized_name");
    IDObj       = json_object_object_get(gameObj, "_id");
    viewersObj  = json_object_object_get(topArrayObj, "viewers");
    channelObj  = json_object_object_get(topArrayObj, "channels");
    gameArray[i].currentViewers  = json_object_get_int(viewersObj);
    gameArray[i].currentChannels = json_object_get_int(channelObj);
    gameArray[i].gameID          = json_object_get_int(IDObj);
    strcpy(gameArray[i].gameName, json_object_get_string(nameObj));
  } 
}

void streamParser(struct streamData stream) {
  printf("filler function");
}

int main() {
  system("mkdir data/");
  queryTwitch();
  struct gameData gameArray[gameLimit];
  topGameParser(gameArray);
  cursesInit(gameArray);
  //Purge created JSON files
  system("rm -rf data/*.json");
  return 0;
}

void cursesInit(struct gameData gameArray[gameLimit]) {
  //setlocale(LC_ALL, "");
  initscr();
  //Disable cursor
  curs_set(0);
  clear();
  refresh();
  start_color();
  int yMax, xMax;
  getmaxyx(stdscr, yMax, xMax);
  //Create window of listed games
  WINDOW *gameList = newwin(yMax, xMax/2, 0, 0);
  WINDOW *channelList = newwin(yMax-2, xMax/2, 0, xMax/2);
  WINDOW *helpMenu = newwin(2, xMax/2, yMax-2, xMax/2);
  refresh();
  printGames(gameList, gameArray);
  printHelp(helpMenu, yMax, xMax);
  char input;
  //Enter loop
  noecho();
  while((input = getch()) != 'q') {
    switch(input) {
      case 'r':
        queryTwitch();
        struct gameData gameArray[gameLimit];
        topGameParser(gameArray);
        printGames(gameList, gameArray);
        mvwprintw(gameList, yMax-botBuffer, leftBuffer, "Refreshed");
        wrefresh(gameList);
    }    
  }
  endwin();
}

void printGames(WINDOW *gameList, struct gameData games[gameLimit]) {
  wclear(gameList);
  int yMax, xMax;
  char sviewers[6];
  getmaxyx(gameList, yMax, xMax);
  mvwprintw(gameList, topBuffer, leftBuffer, "GAME LIST");
  mvwprintw(gameList, topBuffer, xMax-7, "VIEWERS");
  for(int i = 0; i < gameLimit; i++){
    sprintf(sviewers, "%d", games[i].currentViewers);
    mvwprintw(gameList, i+2, leftBuffer, games[i].gameName);
    mvwprintw(gameList, i+2, xMax-7, sviewers);
  }
  wrefresh(gameList);
}

void printHelp(WINDOW *helpMenu, int wyMax, int wxMax) {
  int yMax, xMax;
  getmaxyx(helpMenu, yMax, xMax);
  mvwprintw(helpMenu, 0, 0, "q: Quit");
  mvwprintw(helpMenu, 1, 0, "s: Search");
  mvwprintw(helpMenu, 0, 15, "r: Refresh");
  wrefresh(helpMenu);
}

int streamlinkConnect(struct streamData stream) {
  char *command;
  command = "streamlink twitch.tv/";
  strcat(command, stream.streamName);
  strcat(command, "best");
  system(command);
  return 0;
}
