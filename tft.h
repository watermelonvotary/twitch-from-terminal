#ifndef TFT_H
#define TFT_H

#define gameLimit 10

//NCurses definitions
#define topBuffer 1
#define botBuffer 1
#define leftBuffer 3
#define rightBuffer 3

struct gameData {
  int  currentViewers;
  int  currentChannels;
  int  gameID;
  char gameName[500];
};

struct streamData {
  int currentViewers;
  char streamName[500];
};

static const char quality[6][11]   = {"best", "720p", "480p", "360p", "worst", "audio_only"}; 
static const char clientIDHeader[] = "Client-ID: nwyeglnkseeevdsoq761mruhtr840m";
//static const char clientIDHeader[] = "Client-ID: caozjg12y6hjop39wx996mxn585yqyk";
static const char acceptHeader[]   = "Accept: application/vnd.twitchtv.v5+json";
static const char topGamesURL[]    = "https://api.twitch.tv/kraken/games/top?Limit=10";

#endif
